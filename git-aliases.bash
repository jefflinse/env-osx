# Git-specific aliases
#

alias d="git diff"
alias ds="git diff --staged"
alias s="git status --short --branch -u"
alias b="git branch"
alias ci="git commit"
alias co="git checkout"
alias pull="git pull"
alias push="git push"

function clone {
  local repo=$1
  shift
  git clone git@github.com:$repo $@
}

function postmerge {
  branch=$(git rev-parse --abbrev-ref HEAD)
  if [[ "$branch" != "master" ]]; then
    git checkout master
    git pull
    git branch -d $branch
    echo
    echo "Remaining branches:"
    git branch
    return
  fi
  echo "On branch master; nothing to do."
}

# Git auto-completion
if [ -f $env_dir/git-completion.bash ]; then
  . $env_dir/git-completion.bash
  
  # Enable auto-completion for Git aliases
  __git_complete co _git_checkout
  __git_complete b _git_branch
fi
