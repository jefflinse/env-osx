# Overall Bash Profile
#

env_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# environment variables
if [[ -f $env_dir/env-vars.bash ]]; then
  . $env_dir/env-vars.bash
fi

# bash aliases
if [[ -f $env_dir/aliases.bash ]]; then
  . $env_dir/aliases.bash
fi

# https://unix.stackexchange.com/questions/124407/what-color-codes-can-i-use-in-my-ps1-prompt
let branchcolor=118;
let dircolor=51;

# Default (no git-prompt.sh available)
export PS1='\n\[\033[38;5;${dircolor}m\]\w\[\033[m\] \$ '

# Add git branch to prompt 
if [[ ! -z $(which git) && -f $env_dir/git-prompt.bash ]]; then
  export GIT_PS1_SHOWDIRTYSTATE=1
  . $env_dir/git-prompt.bash
  PROMPT_COMMAND='__git_ps1 "\n" "\[\e[38;5;${dircolor}m\]\w\[\e[m\] \$ " "\[\e[38;5;${branchcolor}m\](%s)\[\e[m\]\n"'
fi

# LS Colors
# Attribute Order:
# 1. directory
# 2. symbolic link
# 3. socket
# 4. pipe
# 5. executable
# 6. block special
# 7. character special
# 8. executable with setuid bit set
# 9. executable with setgid bit set
# 10. directory writable to others, with sticky bit
# 11. directory writable to others, without sticky bit
# Colors:
# a black
# b red
# c green
# d brown
# e blue
# f magenta
# g cyan
# h light grey
# A bold black, usually shows up as dark grey
# B bold red
# C bold green
# D bold brown, usually shows up as yellow
# E bold blue
# F bold magenta
# G bold cyan
# H bold light grey; looks like bright white
# x default foreground or background
# Default is exfxcxdxbxegedabagacad
export LSCOLORS=Exfxcxdxbxegedabagacad

if [[ -d ~/bin ]]; then
  export PATH=$PATH:~/bin
fi

# In case aliases can't be loaded, explicitly call 'ls' here for myenv
alias myenv="pushd $env_dir && ls -F -G -log"
alias loadenv=". $env_dir/profile.bash"
