# Bash aliases
#

# 'cd' shortcuts
alias cd..="cd .."
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."
alias .......="cd ../../../../../.."

# 'ls' shortcuts
alias ls="ls -F -G"
alias l="ls  -log"
alias ll="l -a"

# change directory and list contents
function c {
  cd $1
  l
}

# Misc. short-form commands
alias cls="clear"
alias p="pushd"
alias p.="pushd ."
alias pd="popd"

# Quick-n-dirty "find file"
function ff() {
  find . -name $1 -print
}

# Quick-n-dirty "find string"
function fs() {
  grep -r --include "$2" $1 .
}

# Git aliases
if [[ ! -z "$(which git)" ]]; then
  . $env_dir/git-aliases.bash
fi

# Misc. tools
alias n="nano"
alias tf="terraform"
alias pyhttp="python -m SimpleHTTPServer"

# Generate timestamps in RFC3339 format
alias 3339='date -u +"%Y-%m-%dT%H:%M:%SZ"'

# Get the SHA256 hash of a file
alias sha="openssl dgst -sha256"

# Generate lowercase UUIDs
function uuid() {
  local count=${1:-1}
  for ((i=0; i<count; i++)); do
    uuidgen | tr '[:upper:]' '[:lower:]'
  done
}
